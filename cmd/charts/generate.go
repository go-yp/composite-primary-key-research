package main

import (
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
)

func main() {
	must(save("PostgreSQL INSERT", "./charts/postgres-insert.png",
		"UNIQUE", points(postgresUniqueInsert),
		"PRIMARY", points(postgresPrimaryInsert),
	))
	must(save("PostgreSQL SELECT", "./charts/postgres-select.png",
		"UNIQUE", points(postgresUniqueSelect),
		"PRIMARY", points(postgresPrimarySelect),
	))
	must(save("MySQL INSERT", "./charts/mysql-insert.png",
		"UNIQUE", points(mysqlUniqueInsert),
		"PRIMARY", points(mysqlPrimaryInsert),
	))
	must(save("MySQL SELECT", "./charts/mysql-select.png",
		"UNIQUE", points(mysqlUniqueSelect),
		"PRIMARY", points(mysqlPrimarySelect),
	))
	must(save("CockroachDB INSERT", "./charts/cockroachdb-insert.png",
		"UNIQUE", points(cockroachdbUniqueInsert),
		"PRIMARY", points(cockroachdbPrimaryInsert),
	))
	must(save("CockroachDB SELECT", "./charts/cockroachdb-select.png",
		"UNIQUE", points(cockroachdbUniqueSelect),
		"PRIMARY", points(cockroachdbPrimarySelect),
	))
	must(save("MSSQL INSERT", "./charts/mssql-insert.png",
		"UNIQUE", points(mssqlUniqueInsert),
		"PRIMARY", points(mssqlPrimaryInsert),
	))
	must(save("MSSQL SELECT", "./charts/mssql-select.png",
		"UNIQUE", points(mssqlUniqueSelect),
		"PRIMARY", points(mssqlPrimarySelect),
	))

	must(save("INSERT PRIMARY", "./charts/all-insert-primary.png",
		"PostgreSQL", points(postgresPrimaryInsert),
		"MySQL", points(mysqlPrimaryInsert),
		"CockroachDB", points(cockroachdbPrimaryInsert),
		"MSSQL", points(mssqlPrimaryInsert),
	))

	must(save("SELECT PRIMARY", "./charts/all-select-primary.png",
		"PostgreSQL", points(postgresPrimarySelect),
		"MySQL", points(mysqlPrimarySelect),
		"CockroachDB", points(cockroachdbPrimarySelect),
		"MSSQL", points(mssqlPrimarySelect),
	))
}

func save(title, file string, vs ...interface{}) error {
	p := plot.New()

	p.Title.Text = title
	p.X.Label.Text = "Iteration"
	p.Y.Label.Text = "Duration"
	p.Y.Min = 0

	err := plotutil.AddLines(p,
		vs...,
	)
	if err != nil {
		return err
	}

	// Save the plot to a PNG file.
	if err := p.Save(8*vg.Inch, 8*vg.Inch, file); err != nil {
		return err
	}

	return nil
}

func points(values [20]float64) plotter.XYs {
	result := make(plotter.XYs, len(values))
	for i := range values {
		result[i] = plotter.XY{
			X: float64(i),
			Y: values[i],
		}
	}

	return result
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
