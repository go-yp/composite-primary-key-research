USE yaaws;

SET STATISTICS TIME ON;

TRUNCATE TABLE user_favorite_companies_v2;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (0 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (0 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (1 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (1 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (2 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (2 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (3 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (3 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (4 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (4 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (5 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (5 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (6 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (6 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (7 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (7 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (8 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (8 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (9 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (9 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (10 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (10 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (11 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (11 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (12 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (12 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (13 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (13 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (14 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (14 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (15 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (15 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (16 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (16 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (17 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (17 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (18 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (18 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT value, 1 + (19 + value * 20) % 10000, '20221105 12:00:00', '20221105 12:00:00', 1
FROM GENERATE_SERIES(1, 1000 * 1000);

SELECT COUNT(*)
FROM user_favorite_companies_v2 UFC
INNER JOIN (
    SELECT value user_id, 1 + (19 + value * 20) % 10000 company_id
    FROM GENERATE_SERIES(1, 1000 * 1000)
) GS ON UFC.user_id = GS.user_id AND UFC.company_id = GS.company_id;

SELECT COUNT(*)
FROM user_favorite_companies_v2;

SELECT COUNT(*)
FROM user_favorite_companies_v2;

TRUNCATE TABLE user_favorite_companies_v2;
