TRUNCATE user_favorite_companies_v1;

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (0 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;


SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (0 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (1 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (1 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (2 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (2 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (3 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (3 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (4 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (4 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (5 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (5 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (6 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (6 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (7 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (7 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (8 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (8 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (9 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (9 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (10 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (10 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (11 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (11 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (12 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (12 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (13 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (13 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (14 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (14 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (15 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (15 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (16 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (16 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (17 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (17 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (18 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (18 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT generate_series, 1 + (19 + generate_series * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM generate_series(1, 1000 * 1000)
ON CONFLICT (user_id, company_id) DO UPDATE
    SET updated_at     = '2022-11-05 12:00:00',
        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT generate_series, 1 + (19 + generate_series * 20) % 10000
    FROM generate_series(1, 1000 * 1000)
);

SELECT COUNT(*)
FROM user_favorite_companies_v1;

SELECT COUNT(*)
FROM user_favorite_companies_v1;

TRUNCATE user_favorite_companies_v1;