SET SESSION profiling_history_size = 100;

SET SESSION cte_max_recursion_depth = 1000000;

SET SESSION profiling = 1;

TRUNCATE user_favorite_companies_v1;

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (0 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (0 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (1 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (1 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (2 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (2 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (3 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (3 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (4 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (4 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (5 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (5 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (6 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (6 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (7 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (7 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (8 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (8 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (9 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (9 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (10 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (10 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (11 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (11 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (12 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (12 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (13 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (13 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (14 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (14 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (15 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (15 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (16 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (16 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (17 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (17 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (18 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (18 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT number, 1 + (19 + number * 20) % 10000, '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = '2022-11-05 12:00:00',
                        favorite_state = TRUE;

SELECT COUNT(*)
FROM user_favorite_companies_v1
WHERE (user_id, company_id) IN (
    SELECT number, 1 + (19 + number * 20) % 10000
    FROM (
             WITH RECURSIVE numbers AS (
                 SELECT 1 AS number
                 UNION ALL
                 SELECT number + 1
                 FROM numbers
                 WHERE number < 1000 * 1000
             )
             SELECT *
             FROM numbers
         ) AS generate_series
);

SELECT COUNT(*)
FROM user_favorite_companies_v1;

SELECT COUNT(*)
FROM user_favorite_companies_v1;

TRUNCATE user_favorite_companies_v1;

SHOW PROFILES;