POSTGRES_DSN="postgresql://pguser:pgpass@localhost:5432/yaaws?sslmode=disable"
MYSQL_DSN="myuser:mypass@tcp(localhost:3306)/yaaws?multiStatements=true"
COCKROACHDB_DSN="postgresql://root@localhost:26257/yaaws?sslmode=disable"
MSSQL_DSN="sqlserver://SA:YAAWS_15122022@localhost:1433?database=yaaws"

up:
	docker-compose up -d

pg:
	docker exec -it cpkr_postgres bash

postgres-v1-test:
	cat ./console/postgres.v1.sql | docker exec -i cpkr_postgres psql -d yaaws -U pguser | tee ./console/postgres.v1.output

postgres-v2-test:
	cat ./console/postgres.v2.sql | docker exec -i cpkr_postgres psql -d yaaws -U pguser | tee ./console/postgres.v2.output

mysql-v1-test:
	cat ./console/mysql.v1.sql | docker exec -i -e MYSQL_PWD=mypass cpkr_mysql mysql --database=yaaws --user=myuser --verbose | tee ./console/mysql.v1.output

mysql-v2-test:
	cat ./console/mysql.v2.sql | docker exec -i -e MYSQL_PWD=mypass cpkr_mysql mysql --database=yaaws --user=myuser --verbose | tee ./console/mysql.v2.output

cockroachdb-v1-test:
	cat ./console/cockroachdb.v1.sql | docker exec -i cpkr_cockroachdb cockroach sql --database=yaaws --insecure | tee ./console/cockroachdb.v1.output

cockroachdb-v2-test:
	cat ./console/cockroachdb.v2.sql | docker exec -i cpkr_cockroachdb cockroach sql --database=yaaws --insecure | tee ./console/cockroachdb.v2.output

mssql-v1-test:
	cat ./console/mssql.v1.sql | docker exec -i cpkr_mssql /opt/mssql-tools/bin/sqlcmd -U SA -P "YAAWS_15122022" | tee ./console/mssql.v1.output

mssql-v2-test:
	cat ./console/mssql.v2.sql | docker exec -i cpkr_mssql /opt/mssql-tools/bin/sqlcmd -U SA -P "YAAWS_15122022" | tee ./console/mssql.v2.output

cockroachdb:
	docker exec -it cpkr_cockroachdb bash

cockroachdb-create-database:
	echo "CREATE DATABASE yaaws;" | docker exec -i cpkr_cockroachdb cockroach sql --insecure
	echo "SHOW DATABASES;" | docker exec -i cpkr_cockroachdb cockroach sql --insecure

mssql-create-database:
	echo "CREATE DATABASE yaaws;" | docker exec -i cpkr_mssql /opt/mssql-tools/bin/sqlcmd -U SA -P "YAAWS_15122022"
	echo "SELECT Name from sys.databases;" | docker exec -i cpkr_mssql /opt/mssql-tools/bin/sqlcmd -U SA -P "YAAWS_15122022"

down:
	docker-compose down

down-with-clear:
	docker-compose down --remove-orphans -v # --rmi=all

# make migrate-pgsql-create NAME=init
migrate-pgsql-create:
	# mkdir -p ./storage/pgsql/schema
	$(eval NAME ?= noname)
	goose -dir ./storage/pgsql/schema -table schema_migrations postgres $(POSTGRES_DSN) create $(NAME) sql

migrate-pgsql-up:
	goose -dir ./storage/pgsql/schema -table schema_migrations postgres $(POSTGRES_DSN) up
migrate-pgsql-redo:
	goose -dir ./storage/pgsql/schema -table schema_migrations postgres $(POSTGRES_DSN) redo
migrate-pgsql-down:
	goose -dir ./storage/pgsql/schema -table schema_migrations postgres $(POSTGRES_DSN) down
migrate-pgsql-reset:
	goose -dir ./storage/pgsql/schema -table schema_migrations postgres $(POSTGRES_DSN) reset
migrate-pgsql-status:
	goose -dir ./storage/pgsql/schema -table schema_migrations postgres $(POSTGRES_DSN) status

# make migrate-mysql-create NAME=init
migrate-mysql-create:
	# mkdir -p ./storage/mysql/schema
	$(eval NAME ?= noname)
	goose -dir ./storage/mysql/schema -table schema_migrations mysql $(MYSQL_DSN) create $(NAME) sql

migrate-mysql-up:
	goose -dir ./storage/mysql/schema -table schema_migrations mysql $(MYSQL_DSN) up
migrate-mysql-redo:
	goose -dir ./storage/mysql/schema -table schema_migrations mysql $(MYSQL_DSN) redo
migrate-mysql-down:
	goose -dir ./storage/mysql/schema -table schema_migrations mysql $(MYSQL_DSN) down
migrate-mysql-reset:
	goose -dir ./storage/mysql/schema -table schema_migrations mysql $(MYSQL_DSN) reset
migrate-mysql-status:
	goose -dir ./storage/mysql/schema -table schema_migrations mysql $(MYSQL_DSN) status

migrate-crsql-up:
	goose -dir ./storage/pgsql/schema -table schema_migrations postgres $(COCKROACHDB_DSN) up
migrate-crsql-redo:
	goose -dir ./storage/pgsql/schema -table schema_migrations postgres $(COCKROACHDB_DSN) redo
migrate-crsql-down:
	goose -dir ./storage/pgsql/schema -table schema_migrations postgres $(COCKROACHDB_DSN) down
migrate-crsql-reset:
	goose -dir ./storage/pgsql/schema -table schema_migrations postgres $(COCKROACHDB_DSN) reset
migrate-crsql-status:
	goose -dir ./storage/pgsql/schema -table schema_migrations postgres $(COCKROACHDB_DSN) status

# make migrate-mssql-create NAME=init
migrate-mssql-create:
	# mkdir -p ./storage/mssql/schema
	$(eval NAME ?= noname)
	goose -dir ./storage/mssql/schema -table schema_migrations mssql $(MSSQL_DSN) create $(NAME) sql

migrate-mssql-up:
	goose -dir ./storage/mssql/schema -table schema_migrations mssql $(MSSQL_DSN) up
migrate-mssql-redo:
	goose -dir ./storage/mssql/schema -table schema_migrations mssql $(MSSQL_DSN) redo
migrate-mssql-down:
	goose -dir ./storage/mssql/schema -table schema_migrations mssql $(MSSQL_DSN) down
migrate-mssql-reset:
	goose -dir ./storage/mssql/schema -table schema_migrations mssql $(MSSQL_DSN) reset
migrate-mssql-status:
	goose -dir ./storage/mssql/schema -table schema_migrations mssql $(MSSQL_DSN) status

migrate-all-reset:
	time make migrate-mysql-reset migrate-mysql-up
	time make migrate-pgsql-reset migrate-pgsql-up
	time make migrate-crsql-reset migrate-crsql-up
	time make migrate-mssql-reset migrate-mssql-up

generate-dbs:
	docker run --rm -v $(shell pwd):/src -w /src kjconroy/sqlc generate

bench:
	go test ./tests/... -v -bench=. -benchmem

go-mod-update:
	go get -u
	go mod tidy
	go mod vendor

csv:
	mkdir -p csv
	touch ./csv/postgres.v1.insert.txt
	touch ./csv/postgres.v2.insert.txt
	touch ./csv/postgres.v1.select.txt
	touch ./csv/postgres.v2.select.txt
	touch ./csv/cockroachdb.v1.insert.txt
	touch ./csv/cockroachdb.v2.insert.txt
	touch ./csv/cockroachdb.v1.select.txt
	touch ./csv/cockroachdb.v2.select.txt
	touch ./csv/mysql.v1.insert.txt
	touch ./csv/mysql.v2.insert.txt
	touch ./csv/mysql.v1.select.txt
	touch ./csv/mysql.v2.select.txt
	touch ./csv/mssql.v1.insert.txt
	touch ./csv/mssql.v2.insert.txt
	touch ./csv/mssql.v1.select.txt
	touch ./csv/mssql.v2.select.txt
