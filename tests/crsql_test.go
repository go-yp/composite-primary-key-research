package tests

import (
	"context"
	"database/sql"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/go-yp/composite-primary-key-research/storage/pgsql/dbs"

	_ "github.com/lib/pq"
	"github.com/stretchr/testify/require"
)

const (
	crsqlDSN = "postgresql://root@localhost:26257/yaaws?sslmode=disable"

	crsqlMaxUsersCount     = 1000 * 1000
	crsqlMaxCompaniesCount = 10 * 1000
)

func BenchmarkCockroachDBUserFavoriteCompaniesV1(b *testing.B) {
	connection, err := sql.Open("postgres", crsqlDSN)
	require.NoError(b, err)
	defer func() {
		require.NoError(b, connection.Close())
	}()

	queries, err := dbs.Prepare(context.Background(), connection)
	require.NoError(b, err)
	defer func() {
		require.NoError(b, queries.Close())
	}()

	require.NoError(b, queries.UserFavoriteCompaniesVersion1Truncate(context.Background()))
	require.NoError(b, queries.UserFavoriteCompaniesVersion2Truncate(context.Background()))

	now := time.Now().UTC()
	i := int32(0)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			i := atomic.AddInt32(&i, 1)

			err := queries.UserFavoriteCompaniesVersion1Upsert(context.Background(), dbs.UserFavoriteCompaniesVersion1UpsertParams{
				UserID:        id(crsqlMaxUsersCount, i),
				CompanyID:     id(crsqlMaxCompaniesCount, i),
				CreatedAt:     now,
				UpdatedAt:     now,
				FavoriteState: true,
			})
			require.NoError(b, err)
		}
	})
}

func BenchmarkCockroachDBUserFavoriteCompaniesV2(b *testing.B) {
	connection, err := sql.Open("postgres", crsqlDSN)
	require.NoError(b, err)
	defer func() {
		require.NoError(b, connection.Close())
	}()

	queries, err := dbs.Prepare(context.Background(), connection)
	require.NoError(b, err)
	defer func() {
		require.NoError(b, queries.Close())
	}()

	require.NoError(b, queries.UserFavoriteCompaniesVersion1Truncate(context.Background()))
	require.NoError(b, queries.UserFavoriteCompaniesVersion2Truncate(context.Background()))

	now := time.Now().UTC()
	i := int32(0)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			i := atomic.AddInt32(&i, 1)

			err := queries.UserFavoriteCompaniesVersion2Upsert(context.Background(), dbs.UserFavoriteCompaniesVersion2UpsertParams{
				UserID:        id(crsqlMaxUsersCount, i),
				CompanyID:     id(crsqlMaxCompaniesCount, i),
				CreatedAt:     now,
				UpdatedAt:     now,
				FavoriteState: true,
			})
			require.NoError(b, err)
		}
	})
}
