-- +goose Up
-- +goose StatementBegin
CREATE TABLE users
(
    id         INT      NOT NULL,
    created_at DATETIME NOT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE companies
(
    id         INT      NOT NULL,
    created_at DATETIME NOT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE user_favorite_companies_v1
(
    id             INT AUTO_INCREMENT,
    user_id        INT      NOT NULL,
    company_id     INT      NOT NULL,
    created_at     DATETIME NOT NULL,
    updated_at     DATETIME NOT NULL,
    favorite_state BOOLEAN  NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (user_id, company_id),
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (company_id) REFERENCES companies (id)
) ENGINE = InnoDB;

CREATE TABLE user_favorite_companies_v2
(
    user_id        INT      NOT NULL,
    company_id     INT      NOT NULL,
    created_at     DATETIME NOT NULL,
    updated_at     DATETIME NOT NULL,
    favorite_state BOOLEAN  NOT NULL,
    PRIMARY KEY (user_id, company_id),
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (company_id) REFERENCES companies (id)
) ENGINE = InnoDB;

SET SESSION cte_max_recursion_depth = 1000000;

INSERT INTO users (id, created_at)
SELECT number, '2022-11-05 12:00:00'
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 1000 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series;

INSERT INTO companies (id, created_at)
SELECT number, '2022-11-05 12:00:00'
FROM (
         WITH RECURSIVE numbers AS (
             SELECT 1 AS number
             UNION ALL
             SELECT number + 1
             FROM numbers
             WHERE number < 10 * 1000
         )
         SELECT *
         FROM numbers
     ) AS generate_series;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE user_favorite_companies_v2;
DROP TABLE user_favorite_companies_v1;
DROP TABLE companies;
DROP TABLE users;
-- +goose StatementEnd
