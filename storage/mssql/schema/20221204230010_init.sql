-- +goose Up
-- +goose StatementBegin
CREATE TABLE users
(
    id         INT      NOT NULL,
    created_at DATETIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE companies
(
    id         INT      NOT NULL,
    created_at DATETIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE user_favorite_companies_v1
(
    id             INT      NOT NULL IDENTITY (1,1),
    user_id        INT      NOT NULL,
    company_id     INT      NOT NULL,
    created_at     DATETIME NOT NULL,
    updated_at     DATETIME NOT NULL,
    favorite_state BIT      NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (user_id, company_id),
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (company_id) REFERENCES companies (id)
);

CREATE TABLE user_favorite_companies_v2
(
    user_id        INT      NOT NULL,
    company_id     INT      NOT NULL,
    created_at     DATETIME NOT NULL,
    updated_at     DATETIME NOT NULL,
    favorite_state BIT      NOT NULL,
    PRIMARY KEY (user_id, company_id),
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (company_id) REFERENCES companies (id)
);

INSERT INTO users (id, created_at)
SELECT value, '20221105 12:00:00'
FROM GENERATE_SERIES(1, 1000 * 1000);

INSERT INTO companies (id, created_at)
SELECT value, '20221105 12:00:00'
FROM GENERATE_SERIES(1, 1000 * 1000);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE user_favorite_companies_v2;
DROP TABLE user_favorite_companies_v1;
DROP TABLE companies;
DROP TABLE users;
-- +goose StatementEnd
