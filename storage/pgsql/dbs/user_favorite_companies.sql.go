// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.15.0
// source: user_favorite_companies.sql

package dbs

import (
	"context"
	"time"
)

const userFavoriteCompaniesVersion1Truncate = `-- name: UserFavoriteCompaniesVersion1Truncate :exec
TRUNCATE TABLE user_favorite_companies_v1
`

func (q *Queries) UserFavoriteCompaniesVersion1Truncate(ctx context.Context) error {
	_, err := q.exec(ctx, q.userFavoriteCompaniesVersion1TruncateStmt, userFavoriteCompaniesVersion1Truncate)
	return err
}

const userFavoriteCompaniesVersion1Upsert = `-- name: UserFavoriteCompaniesVersion1Upsert :exec
INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
VALUES ($1, $2, $3, $4, $5)
ON CONFLICT (user_id, company_id) DO UPDATE SET updated_at     = $4,
                                                favorite_state = $5
`

type UserFavoriteCompaniesVersion1UpsertParams struct {
	UserID        int32
	CompanyID     int32
	CreatedAt     time.Time
	UpdatedAt     time.Time
	FavoriteState bool
}

func (q *Queries) UserFavoriteCompaniesVersion1Upsert(ctx context.Context, arg UserFavoriteCompaniesVersion1UpsertParams) error {
	_, err := q.exec(ctx, q.userFavoriteCompaniesVersion1UpsertStmt, userFavoriteCompaniesVersion1Upsert,
		arg.UserID,
		arg.CompanyID,
		arg.CreatedAt,
		arg.UpdatedAt,
		arg.FavoriteState,
	)
	return err
}

const userFavoriteCompaniesVersion2Truncate = `-- name: UserFavoriteCompaniesVersion2Truncate :exec
TRUNCATE TABLE user_favorite_companies_v2
`

func (q *Queries) UserFavoriteCompaniesVersion2Truncate(ctx context.Context) error {
	_, err := q.exec(ctx, q.userFavoriteCompaniesVersion2TruncateStmt, userFavoriteCompaniesVersion2Truncate)
	return err
}

const userFavoriteCompaniesVersion2Upsert = `-- name: UserFavoriteCompaniesVersion2Upsert :exec
INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
VALUES ($1, $2, $3, $4, $5)
ON CONFLICT (user_id, company_id) DO UPDATE SET updated_at     = $4,
                                                favorite_state = $5
`

type UserFavoriteCompaniesVersion2UpsertParams struct {
	UserID        int32
	CompanyID     int32
	CreatedAt     time.Time
	UpdatedAt     time.Time
	FavoriteState bool
}

func (q *Queries) UserFavoriteCompaniesVersion2Upsert(ctx context.Context, arg UserFavoriteCompaniesVersion2UpsertParams) error {
	_, err := q.exec(ctx, q.userFavoriteCompaniesVersion2UpsertStmt, userFavoriteCompaniesVersion2Upsert,
		arg.UserID,
		arg.CompanyID,
		arg.CreatedAt,
		arg.UpdatedAt,
		arg.FavoriteState,
	)
	return err
}
