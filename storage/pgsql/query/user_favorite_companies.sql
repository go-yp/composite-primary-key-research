-- name: UserFavoriteCompaniesVersion1Truncate :exec
TRUNCATE TABLE user_favorite_companies_v1;

-- name: UserFavoriteCompaniesVersion2Truncate :exec
TRUNCATE TABLE user_favorite_companies_v2;

-- name: UserFavoriteCompaniesVersion1Upsert :exec
INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
VALUES (@user_id, @company_id, @created_at, @updated_at, @favorite_state)
ON CONFLICT (user_id, company_id) DO UPDATE SET updated_at     = @updated_at,
                                                favorite_state = @favorite_state;

-- name: UserFavoriteCompaniesVersion2Upsert :exec
INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
VALUES (@user_id, @company_id, @created_at, @updated_at, @favorite_state)
ON CONFLICT (user_id, company_id) DO UPDATE SET updated_at     = @updated_at,
                                                favorite_state = @favorite_state;
