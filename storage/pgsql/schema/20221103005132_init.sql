-- +goose Up
-- +goose StatementBegin
CREATE TABLE users
(
    id         INT,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE companies
(
    id         INT,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE user_favorite_companies_v1
(
    id             SERIAL,
    user_id        INT                      NOT NULL,
    company_id     INT                      NOT NULL,
    created_at     TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at     TIMESTAMP WITH TIME ZONE NOT NULL,
    favorite_state BOOLEAN                  NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (user_id, company_id),
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (company_id) REFERENCES companies (id)
);

CREATE TABLE user_favorite_companies_v2
(
    user_id        INT                      NOT NULL,
    company_id     INT                      NOT NULL,
    created_at     TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at     TIMESTAMP WITH TIME ZONE NOT NULL,
    favorite_state BOOLEAN                  NOT NULL,
    PRIMARY KEY (user_id, company_id),
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (company_id) REFERENCES companies (id)
);

INSERT INTO users (id, created_at)
SELECT *, '2022-11-05 12:00:00 +00:00'
FROM generate_series(1, 1000 * 1000);

INSERT INTO companies (id, created_at)
SELECT *, '2022-11-05 12:00:00 +00:00'
FROM generate_series(1, 10 * 1000);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE user_favorite_companies_v2;
DROP TABLE user_favorite_companies_v1;
DROP TABLE companies;
DROP TABLE users;
-- +goose StatementEnd
