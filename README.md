# Composite primary key research

### DOU
* [Дослідження швидкодії складеного первинного ключа в Postgres, MySQL та CockroachDB](https://dou.ua/forums/topic/40760/)

### Usage
```bash
make up
make cockroachdb-create-database
make migrate-all-reset
```
```bash
go test ./tests/... -v -bench=. -benchmem
go test ./tests/... -v -bench=. -benchmem -benchtime=10s
go test ./tests/... -v -bench=. -benchmem -benchtime=100s
```
```bash
make down
```

### Result
```bash
go test ./tests/... -v -bench=MySQL -benchmem -benchtime=10s
go test ./tests/... -v -bench=Postgres -benchmem -benchtime=10s
go test ./tests/... -v -bench=CockroachDB -benchmem -benchtime=10s
```
```text
time=10s
goos: linux
goarch: amd64
pkg: gitlab.com/go-yp/composite-primary-key-research
cpu: Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz
BenchmarkMySQLUserFavoriteCompaniesV1    	       14757	    876870 ns/op	    1363 B/op	      20 allocs/op
BenchmarkMySQLUserFavoriteCompaniesV2    	       14302	    861288 ns/op	    1400 B/op	      20 allocs/op
BenchmarkPostgresUserFavoriteCompaniesV1    	   35335	    393284 ns/op	    3555 B/op	      50 allocs/op
BenchmarkPostgresUserFavoriteCompaniesV2    	   33271	    405893 ns/op	    3601 B/op	      51 allocs/op
BenchmarkCockroachDBUserFavoriteCompaniesV1    	    1419	   8574190 ns/op	     902 B/op	      21 allocs/op
BenchmarkCockroachDBUserFavoriteCompaniesV2    	    1226	  10713320 ns/op	     916 B/op	      21 allocs/op
```
```bash
go test ./tests/... -v -bench=MySQL -benchmem -benchtime=100s
go test ./tests/... -v -bench=Postgres -benchmem -benchtime=100s
go test ./tests/... -v -bench=CockroachDB -benchmem -benchtime=100s
```
```text
goos: linux
goarch: amd64
pkg: gitlab.com/go-yp/composite-primary-key-research
cpu: Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz
BenchmarkMySQLUserFavoriteCompaniesV1    	      156642	    856566 ns/op	    1488 B/op	      21 allocs/op
BenchmarkMySQLUserFavoriteCompaniesV2    	      146344	    917464 ns/op	    1458 B/op	      20 allocs/op
BenchmarkPostgresUserFavoriteCompaniesV1    	  345070	    405036 ns/op	    3654 B/op	      51 allocs/op
BenchmarkPostgresUserFavoriteCompaniesV2    	  326006	    426527 ns/op	    3632 B/op	      51 allocs/op
BenchmarkCockroachDBUserFavoriteCompaniesV1    	   14571	   9617696 ns/op	     740 B/op	      19 allocs/op
BenchmarkCockroachDBUserFavoriteCompaniesV2    	  194818	    545702 ns/op	    1370 B/op	      26 allocs/op
```

### CockroachDB
* [Client Connection Parameters](https://www.cockroachlabs.com/docs/stable/connection-parameters.html)

### YugabyteDB
* [Build a Go application](https://docs.yugabyte.com/preview/develop/build-apps/go/cloud-ysql-go/)

### Testing MySQL from console
```sql
TRUNCATE user_favorite_companies_v1;

SET SESSION cte_max_recursion_depth = 1000000;

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT 1000000 - (number % 1000000) + 1, 1024 - (number % 1024), '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
       WITH RECURSIVE numbers AS (
           SELECT 1 AS number
           UNION ALL
           SELECT number + 1
           FROM numbers
           WHERE number < 1000 * 1000
       )
       SELECT *
       FROM numbers
   ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = VALUES(updated_at),
                      favorite_state = VALUES(favorite_state);
-- [2022-11-04 00:27:12] 1,000,000 rows affected in 18 s 558 ms

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT 3000000 - (number % 1000000) + 1, 2 * 1024 - (number % 1024), '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
       WITH RECURSIVE numbers AS (
           SELECT 1 AS number
           UNION ALL
           SELECT number + 1
           FROM numbers
           WHERE number < 1000 * 1000
       )
       SELECT *
       FROM numbers
   ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = VALUES(updated_at),
                      favorite_state = VALUES(favorite_state);
-- [2022-11-04 00:27:32] 1,000,000 rows affected in 20 s 147 ms

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT 2000000 - (number % 1000000) + 1, 3 * 1024 - (number % 1024), '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
       WITH RECURSIVE numbers AS (
           SELECT 1 AS number
           UNION ALL
           SELECT number + 1
           FROM numbers
           WHERE number < 1000 * 1000
       )
       SELECT *
       FROM numbers
   ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = VALUES(updated_at),
                      favorite_state = VALUES(favorite_state);
-- [2022-11-04 00:27:51] 1,000,000 rows affected in 18 s 405 ms

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT 5000000 - (number % 1000000) + 1, 4 * 1024 - (number % 1024), '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
       WITH RECURSIVE numbers AS (
           SELECT 1 AS number
           UNION ALL
           SELECT number + 1
           FROM numbers
           WHERE number < 1000 * 1000
       )
       SELECT *
       FROM numbers
   ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = VALUES(updated_at),
                      favorite_state = VALUES(favorite_state);
-- [2022-11-04 00:28:09] 1,000,000 rows affected in 18 s 391 ms

INSERT INTO user_favorite_companies_v1 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT 4000000 - (number % 1000000) + 1, 5 * 1024 - (number % 1024), '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
       WITH RECURSIVE numbers AS (
           SELECT 1 AS number
           UNION ALL
           SELECT number + 1
           FROM numbers
           WHERE number < 1000 * 1000
       )
       SELECT *
       FROM numbers
   ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = VALUES(updated_at),
                      favorite_state = VALUES(favorite_state);
-- [2022-11-04 00:28:31] 1,000,000 rows affected in 21 s 659 ms

SELECT COUNT(*)
FROM user_favorite_companies_v1;
```

```sql
TRUNCATE user_favorite_companies_v2;

SET SESSION cte_max_recursion_depth = 1000001;

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT 1000000 - (number % 1000000) + 1, 1024 - (number % 1024), '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
       WITH RECURSIVE numbers AS (
           SELECT 1 AS number
           UNION ALL
           SELECT number + 1
           FROM numbers
           WHERE number < 1000 * 1000
       )
       SELECT *
       FROM numbers
   ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = VALUES(updated_at),
                      favorite_state = VALUES(favorite_state);
-- [2022-11-04 00:39:15] 1,000,000 rows affected in 19 s 461 ms

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT 3000000 - (number % 1000000) + 1, 2 * 1024 - (number % 1024), '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
       WITH RECURSIVE numbers AS (
           SELECT 1 AS number
           UNION ALL
           SELECT number + 1
           FROM numbers
           WHERE number < 1000 * 1000
       )
       SELECT *
       FROM numbers
   ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = VALUES(updated_at),
                      favorite_state = VALUES(favorite_state);
-- [2022-11-04 00:39:35] 1,000,000 rows affected in 19 s 237 ms

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT 2000000 - (number % 1000000) + 1, 3 * 1024 - (number % 1024), '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
       WITH RECURSIVE numbers AS (
           SELECT 1 AS number
           UNION ALL
           SELECT number + 1
           FROM numbers
           WHERE number < 1000 * 1000
       )
       SELECT *
       FROM numbers
   ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = VALUES(updated_at),
                      favorite_state = VALUES(favorite_state);
-- [2022-11-04 00:39:54] 1,000,000 rows affected in 19 s 29 ms

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT 5000000 - (number % 1000000) + 1, 4 * 1024 - (number % 1024), '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
       WITH RECURSIVE numbers AS (
           SELECT 1 AS number
           UNION ALL
           SELECT number + 1
           FROM numbers
           WHERE number < 1000 * 1000
       )
       SELECT *
       FROM numbers
   ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = VALUES(updated_at),
                      favorite_state = VALUES(favorite_state);
-- [2022-11-04 00:40:16] 1,000,000 rows affected in 22 s 362 ms

INSERT INTO user_favorite_companies_v2 (user_id, company_id, created_at, updated_at, favorite_state)
SELECT 4000000 - (number % 1000000) + 1, 5 * 1024 - (number % 1024), '2022-11-05 12:00:00', '2022-11-05 12:00:00', TRUE
FROM (
       WITH RECURSIVE numbers AS (
           SELECT 1 AS number
           UNION ALL
           SELECT number + 1
           FROM numbers
           WHERE number < 1000 * 1000
       )
       SELECT *
       FROM numbers
   ) AS generate_series
ON DUPLICATE KEY UPDATE updated_at     = VALUES(updated_at),
                      favorite_state = VALUES(favorite_state);
-- [2022-11-04 00:40:34] 1,000,000 rows affected in 17 s 950 ms

SELECT COUNT(*)
FROM user_favorite_companies_v2;
```
